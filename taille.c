#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

int taille(char *path);

int main(int argc, char *argv[]) {
	if(argc == 1) {
		fprintf(stderr, "Usage: %s <pathname>\n", argv[0]);
		return 1;
	}
	for(int i = 1; i < argc; i++) {
		char *path = argv[i];
		printf("%d %s\n", taille(path)/1024, path);
	}

	return 0;
}


int taille(char *path) {
	struct stat buf;
	stat(path, &buf);

	if(!S_ISDIR(buf.st_mode))
		return buf.st_size;

	int size;
	struct dirent *file;
	DIR *root = opendir(path);

	if(root == NULL)
		return 0;

	int status, *size_array, size_array_t = 0;
	pid_t pid, wpid;

	while((file = readdir(root)) != NULL) {
		if(strcmp(file->d_name, ".") && strcmp(file->d_name, ".."))  {

			pid = fork();
			if(pid == 0) {
				// child
				printf("foo");
				char new_path[256];
				sprintf(new_path, "%s/%s", path, file->d_name);
				size_array[size_array_t++] = taille(new_path);
				exit(EXIT_SUCCESS);
			}
			while((wpid = wait(&status) > 0)) {

			}
		}
	}

	closedir(root);

	if(pid > 0) {
		for(int i = 0; i < size_array_t; i++)
			size += size_array[i];
		printf("size: %d\n", size);
	}

	return size;
}
